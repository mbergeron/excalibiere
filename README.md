# Excalibière
Excalibière est un jeu festif de déduction basé sur les Chevaliers de la Table Ronde.

## Histoire
  - Les chevaliers sont très saoul
  - Un évènement se produit et les chevaliers doivent absolument retrouver Excalibur, mais il ne se rappelle pas de quoi elle a l'air.

## But du jeu
Trouver Excalibur.

## Mise en place
  - Chaque joueur incarne un chevalier de la table ronde
  - Un des joueurs est désigné en tant que roi Arthur (un son se fera entendre "Tut tututu!") pour annoncer Arthur.  
  - Le jeu génère l'Excalibur en combinant 3 composantes de manière aléatoire
  - Une caractéristique d'effet est associée aléatoirement à chaque composantes (Voir effets plus bas)
  - Le jeu propose une épée de départ aléatoire au chevaliers
  
### Arthur
Arthur est n'a aucun pouvoir extraordinaire. Il s'agit d'un chevalier comme les autres, outre que:

  - Il commence la partie
  - Il fait partie des cibles potentielle (voir Cible plus bas)
  - Tous les chevaliers lui doivent respect, en l'appellant **Messire**, ou **Sire Arthur** lorsqu'ils s'adressent à lui.  
    Enfreindre cette règle oblige le fautif à boire 1 gorgé.

## Tour du joueur
Le chevalier recoit l'épée du joueur précédent (ou celle de départ si début de partie)

> L'effet de chaque composante est visible, ainsi que l'indicateur d'avancement (# composantes correctes)

1. Modifier l'épée actuelle
2. Manier l'épée
3. S'assurer que tous les chevaliers respectent l'effet

## Maniement de l'épée
Lorsqu'un joueur manie une épée, l'effet de chaques composantes est dévoilé pour le chevalier l'ayant manié. Celui-ci pourra donc connaître l'effet d'une composantes lors d'un tour subsequent, par exemple.

Ensuite, l'indicateur d'avancement indiquera au chevalier ayant manié l'épée **combien de composantes actuelles font partie d'Excalibur**.

## Composantes de l'épée
Chacun des composants s'ajoute au nom de l'épée qui se doit d'être humoristique.
- **Pommeaux** (Préfixe, i.e. Afuttée, Tranchante, Légère, …)
- **Gardes** (Suffixe, i.e. Brûlante, de la Mort, Brisée, …)
- **Lames** (Type d'épée, i.e. Sabre, Rapière, …)

## Effets spéciaux
L'effet est l'enssemble des caractéristiques de chacune des 3 composantes de l'épée

#### Type d'effet (Pommeau)
  - Soif (50%): Bois _X_ gorgées
  - Malédiction (10%): Double `Soif` pendant _X_ tour ou annule `Benediction`
  - Bénédiction (10%): Ne peut être ciblé pendant _X_ tour ou annule `Malédiction`
  - Sommeil (10%): Passe _X_ tour(s)
  - Fanatique (10%): Joue _X_ tour(s) de suite
  - Amour (10%): Tous les amoureux doivent boirent de concert. Dure _X_ tours.

#### Cible (Garde)
- Au choix (30%): Le joueur qui manie l'épée décide le chevalier affecté.
- Joueur (30%): Le joueur qui manie l'épée est affecté.
- Compagnons (20%): Les joueurs assis à gauche et à droite du joueur qui manie l'épée sont affectés.
- Arthur (10%): Seulement Arthur est affecté.

#### Puissance (Lame)
- 1 (30%)
- 2 (30%)
- 3 (20%)
- 4 (10%)
- 5 (10%)

## Règles supplémentaires
- Tous les effets qui durent _X_ tours ne sont pas cummulables.  
  > Exemple: Si un joueur, qui est présentement affecté par `Malédiction` pour deux tours, reçoit à nouveau `Malédiction`: cela n'aura aucun effet sur son nombre de tour restant.