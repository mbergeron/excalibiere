extends Button

export(String) var player_name;
export(Color) var player_color;

onready var texture = $MarginContainer/HBoxContainer/TextureRect
onready var label = $MarginContainer/HBoxContainer/Label

func _ready():
	if player_name:
		label.set_text(player_name)
	if player_color:
		texture.set_modulate(player_color)
