extends ScrollContainer

const THRESHOLD = 1.5;
const PART_TEXTURES = [
	preload("assets/textures/blade.png"),
	preload("assets/textures/guard.png"),
	preload("assets/textures/pummel.png"),
]

enum PartType { 
	BLADE = 0, 
	GUARD = 1, 
	PUMMEL = 2 
}

export(PartType) var part_type;
export(PackedScene) var Component;

onready var list = $components;

var palette = ArrayHelper.shuffle(GameData.COLORS)  # indirect palette

func create_component(idx):
	var c = Component.instance()
	c.set_texture(PART_TEXTURES[part_type])
	c.set_modulate(GameData.COLORS[palette[idx]])
	c.selected = false
	print(idx)
	c.connect("component_selected", self, "_on_component_selected", [idx])
	return c

func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")
	var components = GameData.COMPONENTS[part_type]
	
	for i in range(components.size()):
		list.add_child(create_component(i))
		
	_game_state_changed()
		
func _game_state_changed():
	var components = list.get_children()
	
	for i in range(components.size()):
		components[i].selected = (i == Game.state['selected_components'][part_type])

func _on_component_selected(idx):
	if not Game.player_using_sword():
		Game.execute("component_selected", [part_type, idx])