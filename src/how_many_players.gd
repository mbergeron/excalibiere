extends Control

onready var grid = $MarginContainer/VBoxContainer/GridContainer
var player_count = -1;

func _ready():
	_connect_player_buttons()

func _connect_player_buttons():
	for i in range(grid.get_child_count()):
		var value = i + 2  # starts at 2 players
		var button = grid.get_child(i)
		button.pressed = false
		button.connect("pressed", self, "_player_button_pressed", [button, value])

func _player_button_pressed(pressed_button, button_value):
	player_count = button_value
	
	for button in grid.get_children():
		if button != pressed_button:
			button.pressed = false

func _on_start_pressed():
	Game.execute("start_game", [player_count])
