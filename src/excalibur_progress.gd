extends ProgressBar

func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")
	_game_state_changed()
	
func _game_state_changed():
	self.value = 0
	var selected_components = Game.selected_components()
	
	if not Game.is_sword_discovered(selected_components):
		return
	
	for i in range(selected_components.size()):
		if selected_components[i] == GameData.EXCALIBUR[i]:
			self.value += 1
