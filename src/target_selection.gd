extends Control

export(PackedScene) var ButtonScene;
onready var buttons_container = $MarginContainer/VBoxContainer/ScrollContainer/VBoxContainer

var targets = [];
signal confirmed;

func _ready():
	for i in range(Game.state['players'].size()):
		if i == Game.state['current_player']:
			continue
		var player = Game.state['players'][i]
		var button = create_button(player['name'], player['color'])
		button.connect("pressed", self, "_player_button_pressed", [button, i])
		buttons_container.add_child(button)

func create_button(name, color):
	var button = ButtonScene.instance()
	button.player_name = name
	button.player_color = color
	return button

func _player_button_pressed(pressed_button, player_index):
	if pressed_button.pressed:
		targets = [player_index]
	else:
		targets = []
	
	for button in buttons_container.get_children():
		if button != pressed_button:
			button.pressed = false

func _on_Button_pressed():
	if targets.size() == 0:
		return
	
	emit_signal("confirmed", targets)
	reset()

func reset():
	targets = []
	for button in buttons_container.get_children():
		button.pressed = false