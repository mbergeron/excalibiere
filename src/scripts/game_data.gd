extends Node

const COLORS = [
  Color("#ffffff"),
  Color("#333333"),
  Color("#8214a0"),
  Color("#00a0fa"),
  Color("#14d2dc"),
  Color("#aa0a3c"),
  Color("#fa7850"),
  Color("#0ab45a"),
  Color("#005ac8"),
  Color("#f0f032"),
  Color("#a0fa82"),
  Color("#fae6be"),
  Color("#006e82"),
  Color("#fa78fa"),
]

const KNIGHTS  = [
	'Arthur',
	'Galahad',
	'Lancelot', 
	'Perceval',
	'Lionel',
	'Gareth',
	'Érec',
	'Gauvain',
	'Hector',
	'Lamorak',
	'Tristan',
	'Bedivere',
	'Caradoc',
	'Mordred',
	'Dagonet'
]

const COMPONENTS = [
	["Decaying", "Broken", "Burning", "Deadly", "Blunt", "Desecrated", "Holy", "Dark"],
	["Sword", "Cimitar", "Rapier", "Glaive", "Dagger", "Knife"],
	["of Fire", "of Storm", "of Joy", "of Death", "of Purity", "of Anal Cleansing", "of Annihiliation", "of the Dark Lord"],
]

enum ComponentType {
	BLADE = 0,
	GUARD = 1,
	POMMEL = 2,
}

enum PlayerStatus {
	NONE,
	CURSED,
	BLESSED,
	ASLEEP,
	ZEALOT,
	LOVESTRUCK,
}

const DEFAULT_PLAYER_STATUS = {'status': PlayerStatus.NONE, 'turns': -1}

const Effect = preload("models/effect.gd")
const RandomMap = preload("res://scripts/utils/random_map.gd")

var EFFECTS_RANDOM_MAPS = [
	RandomMap.new(), # Effect type
	RandomMap.new(), # Effect power
	RandomMap.new(), # Effect target
]

var EFFECTS = [[], [], []]

enum SwordRarity {
	MAGIC = 0,
	RARE = 1,
	UNIQUE = 2,
	EXCALIBUR = 3,
}

var SWORD_RARITY_RANDOM_MAP = RandomMap.new()
var SWORD_RARITY = null

var EXCALIBUR = [-1, -1, -1]

# pairing function to get unique combination over
# the component indices
func pair(vals=[]):
	match vals:
		[var a, var b]:
			if max(a, b) == a:
				return a*a + a + b
			else:
				return b*b + a
		_: 
			var last = vals.pop_back()
			return pair([pair(vals), last])

func setup():
	# Effect type
	EFFECTS_RANDOM_MAPS[0].register_odds(5, Effect.Type.THIRST)
	EFFECTS_RANDOM_MAPS[0].register_odds(1, Effect.Type.BLESS)
	EFFECTS_RANDOM_MAPS[0].register_odds(1, Effect.Type.CURSE)
	EFFECTS_RANDOM_MAPS[0].register_odds(1, Effect.Type.SLEEP)
	EFFECTS_RANDOM_MAPS[0].register_odds(1, Effect.Type.ZEAL)
	EFFECTS_RANDOM_MAPS[0].register_odds(1, Effect.Type.LOVE)

	# Effect power
	EFFECTS_RANDOM_MAPS[1].register_odds(3, 1)
	EFFECTS_RANDOM_MAPS[1].register_odds(3, 2)
	EFFECTS_RANDOM_MAPS[1].register_odds(2, 3)
	EFFECTS_RANDOM_MAPS[1].register_odds(1, 4)
	EFFECTS_RANDOM_MAPS[1].register_odds(1, 5)
	
	# Effect target
	EFFECTS_RANDOM_MAPS[2].register_odds(30, Effect.Target.TARGET)
	EFFECTS_RANDOM_MAPS[2].register_odds(30, Effect.Target.SELF)
	EFFECTS_RANDOM_MAPS[2].register_odds(15, Effect.Target.GLORY)
	EFFECTS_RANDOM_MAPS[2].register_odds(15, Effect.Target.MATES)
	# EFFECTS_RANDOM_MAPS[2].register_odds(10, Effect.Target.ARTHUR)
	
	SWORD_RARITY_RANDOM_MAP.register_odds(60, SwordRarity.MAGIC)
	SWORD_RARITY_RANDOM_MAP.register_odds(30, SwordRarity.RARE)
	SWORD_RARITY_RANDOM_MAP.register_odds(10, SwordRarity.UNIQUE)
	
	# seed the effects maps, per rarity
	for r in range(EFFECTS.size()):
		for random_map in GameData.EFFECTS_RANDOM_MAPS:
			EFFECTS[r].push_back(ArrayHelper.shuffle(random_map.odds))
			
	# seed the sword rarity
	SWORD_RARITY = ArrayHelper.shuffle(SWORD_RARITY_RANDOM_MAP.odds)
	
	# seed Excalibur
	for i in range(COMPONENTS.size()):
		randomize()
		EXCALIBUR[i] = randi() % COMPONENTS[i].size()
	
	print("Excalibur is %s %s %s" % EXCALIBUR)