extends Control


func _on_select_sword_pressed():
	if not Game.player_using_sword():
		Game.execute("use_sword", [Game.state["selected_components"]])
	else:
		if Game.excalibur_selected():
			Game.execute("new_game")
		else:
			Game.execute("next_turn")
		
	# this is a shitty way to handle the endgame
	if Game.excalibur_selected():
		$ui/VBoxContainer/PanelContainer/sword_detail/endgame_text.show()
		$ui/VBoxContainer/PanelContainer/sword_detail/sword_effects.hide()

func _on_sword_effects_effect_checked(effect_idx):
	var effect = Game.current_effects()[effect_idx]
	
	var targets = effect.get_targets($popup/target_selection)
	Game.execute("apply_effect", [effect_idx, targets])

func _on_turn_end():
	Game.execute("next_turn")