extends Label

var player = null setget set_player
var turn = null

func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")
	self.mouse_filter = MOUSE_FILTER_PASS
	_game_state_changed()

func _game_state_changed():
	if turn != Game.state['turn']:
		turn = Game.state['turn']
		self.player = Game.current_player()
		
func set_player(value):
	player = value
	text = player["name"]
	$player_change_popup.dialog_text = "PASS THE DEVICE TO YOUR LEFT.\n%s TO PLAY." % player["name"]
	$player_change_popup.popup()