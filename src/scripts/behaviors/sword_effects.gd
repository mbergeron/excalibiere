extends VBoxContainer

const EffectScene = preload("res://effect_control.tscn")
signal effect_checked;


func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")

func _game_state_changed():
	if targets_checked():
		return
	
	for child in get_children():
		# we need to use immediate free to make sure 
		# we won't add too many elements to the effects
		# and make the UI overflow 
		child.free()
	
	if not Game.is_sword_discovered(Game.selected_components()):
		return
		
	for idx in range(Game.current_effects().size()):
		var scene = EffectScene.instance()
		scene.effect_idx = idx
		scene.connect("checked", self, "_on_effect_checked", [idx])
		add_child(scene)

func targets_checked():
	for effect_targets in Game.state['effects_targets']:
		if effect_targets.size() > 0:
			return true
	return false

func _on_effect_checked(effect_idx):
	emit_signal("effect_checked", effect_idx)