extends Label

var player

func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")
	_game_state_changed()

func component_name(type, index):
	return GameData.COMPONENTS[type][index] if index in player["inventory"][type] else "???"

func _game_state_changed():
	player = Game.current_player()
	var selected = Game.selected_components()
	var rarity = Game.current_rarity()
	
	var shown_name = [
		component_name(0, selected[0]),
		component_name(1, selected[1]),
		component_name(2, selected[2]),
	]	
	self.text = "%s %s %s" % shown_name

	if Game.is_sword_discovered(selected):
		if Game.excalibur_selected():
			self.text = "EXCALIBUR"
		
		self.set_font_color(get_rarity_color(rarity))
	else:
		self.set_font_color(Color('ffffff'))

func set_font_color(color):
	set("custom_colors/font_color", color)

func get_rarity_color(rarity):
	match rarity:
		GameData.SwordRarity.MAGIC:
			return Color('#4169E1')
		GameData.SwordRarity.RARE:
			return Color('#CCCC52')
		GameData.SwordRarity.UNIQUE:
			return Color('#A59263')
		GameData.SwordRarity.EXCALIBUR:
			return Color('#FFA500')