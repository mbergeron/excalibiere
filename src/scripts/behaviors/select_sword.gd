extends Button

func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")
	_game_state_changed()

func _game_state_changed():
	self.disabled = false
	
	if Game.player_using_sword():
		if Game.excalibur_selected():
			self.text = "NEW GAME"
		else:
			self.text = 'END TURN'
			self.disabled = have_pending_effects()
	elif Game.is_sword_discovered(Game.selected_components()):
		self.text = 'USE'
	else:
		self.text = 'IDENTIFY'

func have_pending_effects():
	for effect_targets in Game.state['effects_targets']:
		if effect_targets.size() == 0:
			return true

	return false
