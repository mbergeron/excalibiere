extends Node

func shuffle(array):
	var indexes = range(array.size())
	var shuffled = []
	var indexList = range(indexes.size())
	for i in range(indexes.size()):
		randomize()
		var x = randi() % indexList.size()
		shuffled.append(indexes[x])
		indexList.remove(x)
		indexes.remove(x)
	return shuffled

func _ready():
	pass
