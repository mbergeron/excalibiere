var _values = []
var odds = [] setget, get_odds

func register_odds(share, value):
	_values.push_back(value)
	var value_idx = _values.size() - 1

	for i in range(share):
		odds.push_back(value_idx)
		
func at(index, shuffled_odds=null):
	if not shuffled_odds:
		shuffled_odds = ArrayHelper.shuffle(odds)
		
	var odd_idx = shuffled_odds[index % shuffled_odds.size()]
	return _values[odds[odd_idx]]
	
func get_odds():
	return odds.duplicate()