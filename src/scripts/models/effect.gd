enum Type {
	NONE = 0,
	THIRST = 1,
	CURSE = 2,
	BLESS = 3,
	SLEEP = 4,
	ZEAL = 5,
	LOVE = 6,
}

enum Target {
	NONE = 0,
	TARGET = 1,
	SELF = 2,
	MATES = 3,
	GLORY = 4,
	ARTHUR = 5,
}

var type = Type.NONE;
var power = 0;
var target = Target.NONE;

func _init(type, power, target):
	self.type = type
	self.power = power
	self.target = target

# target_selector should be a `target_selection.tscn`
# in a Popup node
func get_targets(target_selector):
	match self.target:
		Target.TARGET:
			# target_selector.dialog
			var popup = target_selector.get_node("..")
			popup.popup()
			var targets = yield(target_selector, "confirmed")
			popup.hide()
			
			return targets # target selector
		Target.GLORY:
			var popup = target_selector.get_node("..")
			# target_selector.dialog
			popup.popup()
			var targets = yield(target_selector, "confirmed")
			popup.hide()
			
			return targets # target selector
		Target.MATES:
			var player_count = Game.state["players"].size()
			var current_index = Game.state["current_player"]
			var right = current_index - 1 if current_index > 0 else player_count
			var left = (current_index + 1) % player_count

			return [right, left]
		Target.ARTHUR:
			return []
		Target.SELF:
			return [Game.state["current_player"]]
		_:
			return []

# this needs to be refactored into classes
# at least `player` should be a model
func apply(player, is_target):
	match self.type:
		Type.THIRST:
			player["to_drink"] = power
		Type.BLESS:
			match player["status"]:
				GameData.PlayerStatus.BLESSED:
					if not is_target:
						player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				GameData.PlayerStatus.CURSED:
					player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				_:
					player["status"] = {"status": GameData.PlayerStatus.BLESSED, "turns": power}
		Type.CURSE:
			match player["status"]:
				GameData.PlayerStatus.CURSED:
					if not is_target:
						player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				GameData.PlayerStatus.BLESSED:
					player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				_:
					player["status"] = {"status": GameData.PlayerStatus.CURSED, "turns": power}
		Type.SLEEP:
			match player["status"]:
				GameData.PlayerStatus.ASLEEP:
					if not is_target:
						player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				_:
					player["status"] = {"status": GameData.PlayerStatus.ASLEEP, "turns": power}
		Type.ZEAL:
			match player["status"]:
				GameData.PlayerStatus.ZEALOT:
					if not is_target:
						player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				_:
					player["status"] = {"status": GameData.PlayerStatus.ZEALOT, "turns": power}
		Type.LOVE:
			match player["status"]:
				GameData.PlayerStatus.LOVESTRUCK:
					if not is_target:
						player["status"] = {"status": GameData.PlayerStatus.NONE, "turns": -1}
				_:
					player["status"] = {"status": GameData.PlayerStatus.LOVESTRUCK, "turns": power}