extends Node

signal game_state_changed
var state = {}

func selected_components():
	return state["selected_components"].duplicate()

func excalibur_selected():
	return state["selected_components"] == GameData.EXCALIBUR

func current_player():
	return state["players"][state["current_player"]]

func current_rarity():
	if excalibur_selected():
		return GameData.SwordRarity.EXCALIBUR
	
	var pair = GameData.pair(selected_components())
	return GameData.SWORD_RARITY_RANDOM_MAP.at(pair, GameData.SWORD_RARITY)

func current_effects():
	var effects = []
	var rarity = current_rarity()
	var pair = GameData.pair(selected_components())

	for i in range(rarity + 1):
		var effect = GameData.Effect.new(
			GameData.EFFECTS_RANDOM_MAPS[0].at(pair, GameData.EFFECTS[i-1][0]),
			GameData.EFFECTS_RANDOM_MAPS[1].at(pair, GameData.EFFECTS[i-1][1]),
			GameData.EFFECTS_RANDOM_MAPS[2].at(pair, GameData.EFFECTS[i-1][2])
		)
		effects.push_back(effect)

	return effects

func is_sword_discovered(selected_components):
	var pair = GameData.pair(selected_components())
	return pair in current_player()['inventory'][3]

func player_using_sword():
	return state['effects_targets'].size() > 0

func build_players(quantity):
	state['players'] = []
	var shuffled_indexes = ArrayHelper.shuffle(GameData.KNIGHTS)
	for i in range(quantity):
		state['players'].append({
			'name': GameData.KNIGHTS[shuffled_indexes[i]],
			'color': GameData.COLORS[i],
			'inventory': [[], [], [], []], #  [blade, guard, pommel, pair]
			'status': GameData.DEFAULT_PLAYER_STATUS,
		})

func execute(action_type, args = []):
	print('executing action %s' % action_type)
	match action_type:
		"new_game":
			get_tree().change_scene('res://how_many_players.tscn')
		"start_game":
			setup()
			var player_count = args[0]
			build_players(player_count)
			get_tree().change_scene('res://selector.tscn')
		"component_selected":
			var component_type = args[0]
			var component_idx = args[1]
			print("selecting component(%s): %s" % args)
			state['selected_components'][component_type] = component_idx
		"apply_effect":
			var effect_idx = args[0]
			var effect = current_effects()[effect_idx]
			var targets = args[1]

			state['effects_targets'][effect_idx].append(targets)

			for i in state["players"].size():
				var is_target = i in targets
				effect.apply(state["players"][i], is_target)
		"use_sword":
			var components = args[0]
			var player = current_player()

			# add this sword to the player's inventory
			for i in components.size():
				player['inventory'][i].push_back(components[i])
			player['inventory'][3].push_back(GameData.pair(components.duplicate()))

			# setup all effects targets
			state['effects_targets'] = []
			for effect in current_effects():
				state['effects_targets'].append([])
		"next_turn":
			state['turn'] += 1
			state['effects_targets'] = []
			state['current_player'] = next_player()

			print("Player to play: %s" % state['current_player'])

	emit_signal("game_state_changed")

func next_player():
	var player = current_player()
	var next_player_idx = state['current_player']

	match player['status']:
		{'status': GameData.PlayerStatus.ZEALOT, 'turns': var turnsLeft}:
			if turnsLeft > 1:
				# consume one turn
				player['status']['turns'] -= 1
				return next_player_idx
			else:
				player['status'] = GameData.DEFAULT_PLAYER_STATUS

	while true:
		next_player_idx = (next_player_idx + 1) % state['players'].size()
		print(next_player_idx)
		player = state['players'][next_player_idx]
		match player['status']:
			{"status": GameData.PlayerStatus.ASLEEP, "turns": var turnsLeft}:
				if turnsLeft > 1:
					# consume one turn
					player['status']['turns'] -= 1
				else:
					# player wakes up
					player['status'] = GameData.DEFAULT_PLAYER_STATUS
					break;
			_:
				break;

	return next_player_idx

func _ready():
	build_players(13)
	
	
func setup():
	GameData.setup()
	
	state = {
		'players': [],
		'selected_components': [0, 0, 0], # refactor: local to the selector
		'current_player': 0,
		'turn': 0,
		'effects_targets': [],
	}