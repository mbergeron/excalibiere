shader_type canvas_item;
render_mode blend_mix; 

uniform float speed = 4.0;

void fragment() {
	vec2 emit_dir = vec2(1, 1) - abs(vec2(UV) - vec2(0.5, 0.5));
	vec4 tx_color = texture(TEXTURE, UV);
	
	float power = length(emit_dir) * (3.0 + sin(TIME * speed));
	//power = 5.0;
	float decay = (length(emit_dir) / 2.0 );
	decay = 1.0 - pow(decay, 0.8);
	
	float opacity = 0.3 + sin(TIME * speed * 2.0) / 10.0;
	vec4 glow = vec4(0.2 * power, 0.2 * power, 0.2 * power, opacity);
	vec3 albedo = mix(tx_color.rgb, glow.rgb, vec3(0.7, 0.7, 0.7));
	//vec3 albedo = glow.rgb;
	COLOR = vec4(albedo, max(tx_color.a, glow.a - decay));
}