extends HBoxContainer

export(int) var effect_idx;

onready var label = $Label
onready var check_button = $check_button_container/CheckButton

signal checked;

func _ready():
	Game.connect("game_state_changed", self, "_game_state_changed")
	_game_state_changed()
	
	if effect_idx < 0:
		return

	var effect = Game.current_effects()[effect_idx]
	var effect_message = "+ %s %s %s" % [effect.power, get_effect_type_text(effect), get_effect_target_text(effect)]
	label.set_text(effect_message)

func _game_state_changed():
	# hide the checkbox until the sword is used/identified
	check_button.visible = len(Game.state['effects_targets']) != 0

func get_effect_type_text(effect):
	match effect.type:
		effect.Type.THIRST:
			return 'THIRST'
		effect.Type.BLESS:
			return 'BLESS'
		effect.Type.CURSE:
			return 'CURSE'
		effect.Type.SLEEP:
			return 'SLEEP'
		effect.Type.ZEAL:
			return 'ZEAL'
		effect.Type.LOVE:
			return 'LOVE'

func get_effect_target_text(effect):
	match effect.target:
		effect.Target.TARGET:
			return 'TARGET'
		effect.Target.GLORY:
			return 'GLORY'
		effect.Target.MATES:
			return 'MATES'
		effect.Target.ARTHUR:
			return 'ARTHUR'
		effect.Target.SELF:
			return 'SELF'

func _on_CheckButton_toggled(button_pressed):
	emit_signal("checked")
	check_button.disabled = true
