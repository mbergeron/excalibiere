tool
extends TextureRect

signal component_selected;

const THRESHOLD = 25
export(bool) var selected = false setget selected_change;
var press_position = Vector2()

func _ready():
	set_process_input(true)
		
func _gui_input(ev):
	if ev is InputEventMouseButton and ev.button_index == BUTTON_LEFT:
		if ev.pressed:
			press_position = ev.position
		elif (ev.position - press_position).length() < THRESHOLD:
			emit_signal("component_selected")
		else:
			print("Too much motion, ignoring selection")
	
func _unhandled_input(ev):
	if ev is InputEventMouseButton and not ev.pressed and ev.button_index == BUTTON_LEFT:
		print("click")
		emit_signal("component_selected")
	
func selected_change(value):
	selected = value
	self.use_parent_material = not selected