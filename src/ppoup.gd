extends Control


const target_select_scene = preload("res://target_selection.tscn")

func _ready():
	var scene = target_select_scene.instance()
	scene.connect("confirmed", self, "_on_Control_confirmed")
	$Popup.add_child(scene)
	$Popup.popup()

func _on_Control_confirmed():
	$Popup.hide()
	for child in $Popup.get_children():
		child.queue_free()
