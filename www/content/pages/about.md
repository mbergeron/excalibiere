---
title: About
date: 2018-03-04
comments: false
---

Excalibière has been developed by Kaven Thériault and Micaël Bergeron using the great [Godot Engine](https://godotengine.org)

Feel free to poke around the [source code](https://gitlab.com/mbergeron/excalibiere) and open issues if you feel like it.

Gather your friends, crack open a couple 🍺🍺🍺 and have some fun.

## License

Excalibière is MIT licensed.

## Credits

The Excalibière logo uses edited graphics designed by 

 - [Freepik](http://www.freepik.com) via http://www.flaticon.com
 - [Lorc](http://lorcblog.blogspot.com) via http://game-icons.net

The game uses edited graphics designed by:

 - [Lorc](http://lorcblog.blogspot.com) via http://game-icons.net
 - [Exoaria](https://opengameart.org/users/exoaria) 
