---
title: How to play?
date: 2019-03-06
---

## Win condition
Be the first player to assemble **Excalibur**.

## Player turn
#### Phase 1 &mdash; Sword selection
- Scroll from left to right through the sword components (knobs, guards and blades)
- Select a component by touching it (the component will glow when selected)
- Click on `IDENTIFY` (or `USE` if the sword is already identified)

#### Phase 2 &mdash; Magic effects
Identifying (or using) the sword will unleash its magical effects. Each sword has 1 to 3 magical effects that the player **must** activate by clicking the red mug. 

Depending on the [effect's](#effects) [target](#targets), the game might ask the player to select the player that will be affected by the effect. 

#### Phase 3 &mdash; End of turn
When all the red mugs are checked, the player can click on `END TURN` to end its turn.

It is the player's last chance to remember the current components and wether they are part or not of Excalibur, using the [Excalibur component indicator](#excalibur-component-indicator).

After the turn ends, the game will indicate who should play next.

## Excalibur component indicator
> The _Excalibur component indicator_ is at the bottom of the screen (long gray bar).

When a sword is identified, the component indicator will light up to show **how many of the selected component are also part of Excalibur**. 

The bar does not indicate which of the components though are part of Excalibur: **it indicates how many there are**.

## Effects
> _X_ stands for the power of the effect.

- **Thirst**: take _X_ sap
- **Sleep**: skip _X_ turns
- **Zeal**: play _X_ more times
- **Curse**: doubles saps for _X_ turns
- **Bless**: halves saps for _X_ turns

## Targets
- **Self**: current player
- **Target**: choose any other player
- **Mates** the players to your left and right
- **Glory** the last player to cheer (yes, you have to cheer.)
