## Excali-what?
<small><i>Excalibière &mdash; the contraction of Excalibur and Bière (french for Beer)</i></small>


Excalibière is an engaging party drinking game meant to be played in gatherings. 

It requires no setup, has very little mechanics but will hopefully bring fun to the table.

It only requires one Android device to play, is open-source and MIT licensed.

